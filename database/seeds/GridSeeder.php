<?php

use Illuminate\Database\Seeder;
use App\Grid;

class GridSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 1',
			'group' 		=> 'Group 1',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 2',
			'group' 		=> 'Group 2',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> false,
			'user' 			=> 'user 3',
			'group' 		=> 'Group 3',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 4',
			'group' 		=> 'Group 4',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 5',
			'group' 		=> 'Group 2',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> false,
			'user' 			=> 'user 6',
			'group' 		=> 'Group 3',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> false,
			'user' 			=> 'user 7',
			'group' 		=> 'Group 3',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 8',
			'group' 		=> 'Group 3',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 9',
			'group' 		=> 'Group 1',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 10',
			'group' 		=> 'Group 1',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
        Grid::create([
            'state' 		=> true,
			'user' 			=> 'user 11',
			'group' 		=> 'Group 1',
			'role' 			=> 'PM',
			'organization' 	=> 'ZS'
        ]);
    }
}
