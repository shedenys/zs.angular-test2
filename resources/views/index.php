<!doctype html>
<html lang="en" ng-app="gridApp">
<head>
	<meta charset="utf-8">
	<title>AngularJS. Тестове завдання</title>
	<link rel="stylesheet" href="/tpl/css/font-awesome.min.css">
	<link rel="stylesheet" href="/tpl/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/tpl/css/style.css"/>
</head>
  
<body>
  
	<div ui-view></div>
  
	<script src="app/lib/jquery.min.js"></script>
	<script src="app/lib/angular/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.18/angular-ui-router.js"></script>
	<script src="app/app.js"></script>
	<script src="app/services/backendService.js"></script>
	<script src="app/services/paginationService.js"></script>

	<script src="app/modules/default/defaultModule.js"></script>
	<script src="app/modules/default/controllers/defaultIndexController.js"></script>

	<script src="app/modules/admin/adminModule.js"></script>
	<script src="app/modules/admin/controllers/adminIndexController.js"></script>
	<script src="app/modules/admin/controllers/adminAddController.js"></script>
	<script src="app/modules/admin/controllers/adminEditController.js"></script>
</body>
</html>