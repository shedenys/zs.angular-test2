<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grid extends Model
{
    protected $table = 'grid';
    protected $guarded = [];
}
