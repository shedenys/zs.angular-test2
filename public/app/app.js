'use strict';

angular.module('gridApp', [
	'ui.router',
	'gridApp.default',
	'gridApp.admin',
	'gridApp.services'
]);

angular.module('gridApp.services', []);

angular.module('gridApp').run(['$state', function($state) {
	$state.go('defaultIndex');
}]);