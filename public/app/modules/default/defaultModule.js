'use strict';

angular.module('gridApp.default',[
	'gridApp.default.controllers',
	'gridApp.default.services',
	// 'spBlogger.posts.directives',
	// 'spBlogger.posts.filters',
]);

angular.module('gridApp.default.controllers', []);
angular.module('gridApp.default.services', []);

angular.module('gridApp.default')

.config(['$stateProvider', '$locationProvider', function($stateProvider, $locationProvider) {
	$stateProvider.state('defaultIndex', {
		url: '/',
		// templateUrl: '',
		templateUrl: 'app/modules/default/views/index.html',
		controller: 'defaultIndexController'
	});
	// $locationProvider.html5Mode(true);
}]);