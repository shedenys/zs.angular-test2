'use strict';

angular.module('gridApp.admin',[
	'gridApp.admin.controllers',
]);

angular.module('gridApp.admin.controllers', []);

angular.module('gridApp.admin')

.config(['$stateProvider', '$locationProvider', function($stateProvider, $locationProvider) {
	$stateProvider.state('adminIndex', {
		url: '/admin',
		templateUrl: 'app/modules/admin/views/index.html',
		controller: 'adminIndexController',
		params: {
			successMessage: ''
		}
	});
	$stateProvider.state('adminAdd', {
		url: '/admin/add',
		templateUrl: 'app/modules/admin/views/add.html',
		controller: 'adminAddController',
	});
	$stateProvider.state('adminEdit', {
		url: '/admin/edit/{id}',
		templateUrl: 'app/modules/admin/views/edit.html',
		controller: 'adminEditController'
	});
	// $locationProvider.html5Mode(true);
}]);