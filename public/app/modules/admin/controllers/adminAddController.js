'use strict';

angular.module('gridApp.admin.controllers')

.controller('adminAddController', ['$scope', '$http', 'backendService', function($scope, $http, backendService) {

	$scope.recordData = {};
	$scope.success = '';
	$scope.danger = '';

	/*
	 * Функція додавання нового запису
	 */
	$scope.addRecord = function() {

		if ($scope.itemForm.$valid) {
			backendService.store($scope.recordData)
				.success(function(data) {
					$scope.recordData = {};
					$scope.success = 'New record successfully added.';

				})
				.error(function(data) {
					console.log(data);
				});
			$scope.danger = '';
		}
		else {
			$scope.danger = 'Unable to save. Validation error!';
			$scope.success = '';
		}

	}

}]);