'use strict';

angular.module('gridApp.admin.controllers')

.controller('adminEditController', ['$scope', '$http', '$state', '$stateParams', 'backendService',
	function($scope, $http, $state, $stateParams, backendService) {

	$scope.recordData = {};
	$scope.danger = '';

	/*
	 * Отримання данних обраного для редагування запису
	 */
	function init(){
		backendService.edit($stateParams.id)
		.success(function(data){
			$scope.recordData = data;
		});
	}

	init();

	/*
	 * Функція збереження змін обраного для редагування запису
	 */
	$scope.editRecord = function(){
		
		if ($scope.itemForm.$valid) {
			console.log('saving user');		// save $scope.user object
			backendService.update( $scope.recordData )
				.success(function(data){
					$state.go('adminIndex', {successMessage: 'Record successfully updated.'});
				});
			$scope.danger = '';
		}
		else {
			$scope.danger = 'Unable to save. Validation error!';
			$scope.success = '';
		}
	}

}]);