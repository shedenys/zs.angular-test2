'use strict';

angular.module('gridApp.services')

	.factory('backendService', function($http) {
		return {
			/*
			 * Функція відправлення запиту на отримання всіх записів з урахуванням сортування, фільтрації та пагінації
			 */
			index: function(orderTarget, orderDirection, perPage, pageNumber, filterData) {
				return $http({
				    url: 'api/items', 
				    method: "GET",
				    params: {otarget: orderTarget, odirection: orderDirection, perpage: perPage, page: pageNumber, fdata: filterData }
				});
			},
			/*
			 * Функція відправлення запиту на збереження новодобавленого запису 
			 */
			store: function(recordData) {
				return $http({
					method: 'POST',
					url: 'api/items',
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(recordData)
				})
			},
			/*
			 * Функція відправлення запиту на видалення обраного запису 
			 */
			destroy : function(id) {
				return $http.delete('api/items/' + id);
			},
			/*
			 * Функція відправлення запиту на отримання даних обраного запису для його редагування
			 */
			edit: function(id){
				return $http.get('/api/items/' + id + '/edit');
			},
			/*
			 * Функція відправлення запиту на оновлення даних обраного запису після його редагування
			 */
			update: function(data){
			 	return $http({
				    url: '/api/items/' + data.id, 
				    method: "PUT",
				    data: data
				});
			}
		}
	});